using PyPlot
using CSV
using DataFrames
using Random


include("../instances_io.jl")
include("../pl_p1.jl")
include("../pl_p2.jl")
include("../pl_p3.jl")
include("../heuristic.jl")

include("simulate_time.jl")
include("simulate_cost.jl")
include("plotting_cost.jl")
include("plotting_time.jl")


function main_plotting(action::String, nb_simulations_lc::Int, nb_simulations_k::Int, nb_simulations_max::Int, maxd_inf::Bool)


	# Plotting ordonate execution time
	if action == "time"

		# does maxd = -1 ?
		if maxd_inf

			# simulating l = c variates
			println("STEP 1/6 --- simulating lc --- in progress...")
			filename, nameid = launch_simulate_time(nb_simulations_lc, params_lc = (6, 1), params_maxd = (-1, 0))
			println("STEP 2/6 --- plotting lc --- in progress...")

			# plotting l = c variates
			plot_time_params("../../res/$filename", :l, [:timeHeur, :timePL1, :timePL2, :timePL3], "$(nameid)_$(nb_simulations_lc)", maxd_inf)
			plot_arcs_nb("../../res/$filename", :l, [:nb_added_arcsPL2, :nb_added_arcsPL3], "$(nameid)_$(nb_simulations_lc)", maxd_inf)

			# simulating k variates
			println("STEP 3/6 --- simulating k --- in progress...")
			filename, nameid = launch_simulate_time(nb_simulations_k, params_k = (0, 1), params_maxd = (-1, 0))
			println("STEP 4/6 --- plotting k --- in progress")

			# plotting k variates
			plot_time_params("../../res/$filename", :k, [:timeHeur, :timePL1, :timePL2, :timePL3], "$(nameid)_$(nb_simulations_k)", maxd_inf)
			plot_arcs_nb("../../res/$filename", :k, [:nb_added_arcsPL2, :nb_added_arcsPL3], "$(nameid)_$(nb_simulations_lc)", maxd_inf)


			# simulating maxc variates
			println("STEP 5/6 --- simulating maxc --- in progress...")
			filename, nameid = launch_simulate_time(nb_simulations_max, params_maxc = (5, 1), params_maxd = (-1, 0))
			println("STEP 6/6 --- plotting maxc --- in progress...")

			# plotting maxc variates
			plot_time_params("../../res/$filename", :maxc, [:timeHeur, :timePL1, :timePL2, :timePL3], "$(nameid)_$(nb_simulations_max)", maxd_inf)
			plot_arcs_nb("../../res/$filename", :maxc, [:nb_added_arcsPL2, :nb_added_arcsPL3], "$(nameid)_$(nb_simulations_lc)", maxd_inf)

			# No simulation for maxd because it is already set here to -1
		else

			# simulating l = c variates
			println("STEP 1/8 --- simulating lc --- in progress...")
			filename, nameid = launch_simulate_time(nb_simulations_lc, params_lc = (6, 1))
			println("STEP 2/8 --- plotting lc --- in progress...")
			plot_time_params("../../res/$filename", :l, [:timeHeur, :timePL1, :timePL2], "$(nameid)_$(nb_simulations_lc)")
			plot_arcs_nb("../../res/$filename", :l, [:nb_added_arcsPL2], "$(nameid)_$(nb_simulations_lc)")

			# simulating k variates
			println("STEP 3/8 --- simulating k --- in progress...")
			filename, nameid = launch_simulate_time(nb_simulations_k, params_k = (0, 1))

			# plotting k variates
			println("STEP 4/8 --- plotting k --- in progress")
			plot_time_params("../../res/$filename", :k, [:timeHeur, :timePL1, :timePL2], "$(nameid)_$(nb_simulations_k)")
			plot_arcs_nb("../../res/$filename", :k, [:nb_added_arcsPL2], "$(nameid)_$(nb_simulations_lc)")

			# simulating maxc variates
			println("STEP 5/8 --- simulating maxc --- in progress...")
			filename, nameid = launch_simulate_time(nb_simulations_max, params_maxc = (10, 5))

			# plotting maxc variates
			println("STEP 6/8 --- plotting maxc --- in progress...")
			plot_time_params("../../res/$filename", :maxc, [:timeHeur, :timePL1, :timePL2], "$(nameid)_$(nb_simulations_max)")
			plot_arcs_nb("../../res/$filename", :maxc, [:nb_added_arcsPL2], "$(nameid)_$(nb_simulations_lc)")

			# simulating maxd variates
			println("STEP 7/8 --- simulating maxd --- in progress...")
			filename, nameid = launch_simulate_time(nb_simulations_max, params_maxd = (10, 5))

			# plotting maxc variates
			println("STEP 8/8 --- plotting maxd --- in progress...")
			plot_time_params("../../res/$filename", :maxd, [:timeHeur, :timePL1, :timePL2], "$(nameid)_$(nb_simulations_max)")
			plot_arcs_nb("../../res/$filename", :maxd, [:nb_added_arcsPL2], "$(nameid)_$(nb_simulations_lc)")
		end

	elseif action == "cost"
		# Number of launches
		nb_same_launch = 10

		# Simulating cost, l = c variates
		println("STEP 1/4... in progress")
		filename, nameid = launch_simulate_cost(nb_simulations_lc, nb_same_launch, params_lc = (6, 1))
		println("STEP 2/4... in progress")

		# Plotting cost, l = c variates
		plot_time_cost("../../res/$filename", :l, :distHeur, "$(nameid)_$(nb_simulations_lc)")

		# Simulating cost, k variates
        println("STEP 3/4... in progress")
        filename, nameid = launch_simulate_cost(nb_simulations_k, nb_same_launch, params_k = (0, 1))

		# Plotting cost, k variates
        println("STEP 4/4... in progress")
        plot_time_cost("../../res/$filename", :k, :distHeur, "$(nameid)_$(nb_simulations_k)")
	end
end


# number of simulations for each paramter to variate (working with CPLEX)
nb_simulations_lc = 10
nb_simulations_k = 9
nb_simulations_max = 10

# You might want to use theses parameters if you use GLPK
# nb_simulations_lc = 3
# nb_simulations_k = 3
# nb_simulations_max = 3

# Does maxd = -1 ?
maxd_inf = true


main_plotting("time", nb_simulations_lc, nb_simulations_k, nb_simulations_max, maxd_inf)
maxd_inf = false
main_plotting("time", nb_simulations_lc, nb_simulations_k, nb_simulations_max, maxd_inf)
main_plotting("cost", nb_simulations_lc, nb_simulations_k, nb_simulations_max, maxd_inf)
