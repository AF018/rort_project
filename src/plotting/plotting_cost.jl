using PyPlot

@doc """
    Function to use to performance of heuristic
    Displays and saves the distance between the optimal solution and the score of the heuristic
"""
function plot_time_cost(filename::String, x_id::Symbol, heur_dist::Symbol,  outputname::String = "")
    # Opens the CSV file
    data = CSV.read(filename, header = true, delim = ',')

    # Plots the evolution of the performane of the heuristic
    fig = figure()
    plot(data[:,x_id], data[:,heur_dist], color = tuple(rand(3,1)...,), label="|opt-cost(heuristic)|/opt")

    # Adds the legend to know which graph corresponds to what
    legend()
    title("Performance of heuristic as function of parameter $(x_id)")
    xlabel("$(x_id)")
    ylabel("|opt-cost(heuristic)|/opt")

    # If a name has been provided, save the figures in a EPS file, under this name
    if length(outputname) > 0
        savefig("../../res/plots/plot_cost_$outputname.eps")
    end
    close(fig)
end
