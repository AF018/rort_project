using PyPlot

@doc """
    Displays and saves the execution time
"""
function plot_time_params(filename::String, x_id::Symbol, y_ids::Array{Symbol,1}, outputname::String = "", maxd_inf::Bool = false)
    # Opens the CSV file
    data = CSV.read(filename, header = true, delim = ',')

    # Plots the evolution of the execution times
    figure()
    for i in 1:length(y_ids)
        plot(data[:,x_id], data[:,y_ids[i]], color = tuple(rand(3,1)...,), label=y_ids[i])
    end
    
    # Adds the legend to know which graph corresponds to what
    legend()
    if maxd_inf
        title("maxd = -1 (Inf)")
    end
    subtitle_string = join(["$(y_ids[i]) " for i in 1:length(y_ids)])
    suptitle("Execution time as a function of parameter $(x_id)")
    xlabel("$(x_id)")
    ylabel("simulation time")

    # If a name has been provided, save the figures in a EPS file, under this name
    if length(outputname) > 0
        savefig("../../res/plots/plot_time_$(outputname)_maxdinf_$(maxd_inf).eps")
    end
end

@doc """
    Displays and saves the number of arcs added for the second and third resolutions
"""
function plot_arcs_nb(filename::String, x_id::Symbol, y_ids::Array{Symbol,1}, outputname::String = "", maxd_inf::Bool = false)
    # Opens the CSV file
    data = CSV.read(filename, header = true, delim = ',')

    # Plots the evolution of the execution times
    fig = figure()
    for i in 1:length(y_ids)
        plot(data[:,x_id], data[:,y_ids[i]], color = tuple(rand(3,1)...,), label=y_ids[i])
    end
    # Adds the legend to know which graph corresponds to what
    legend()
    if maxd_inf
        title("maxd = -1 (Inf)")
    end
    subtitle_string = join(["$(y_ids[i]) " for i in 1:length(y_ids)])
    suptitle("Number of added arcs as a function of parameter $(x_id)")
    xlabel("$(x_id)")
    ylabel("Number of added arcs")

    # If a name has been provided, save the figures in a EPS file, under this name
    if length(outputname) > 0
        savefig("../../res/plots/plot_arcs_$(outputname)_maxdinf_$(maxd_inf).eps")
    end
    close(fig)
end
