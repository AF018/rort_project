using CSV
using DataFrames
using ProgressMeter

include("../instances_io.jl")
include("../pl_p1.jl")
include("../pl_p2.jl")
include("../pl_p3.jl")
include("../heuristic.jl")

@doc """
    Completes the simulations recording the execution times and saves the results in a CSV
    - All arrays in argument must have same length
    - nameid will be the end of output filename
"""
function simulate_time(l_arr::Array{Int,1}, c_arr::Array{Int,1}, k_arr::Array{Int,1}, maxc_arr::Array{Int,1}, maxd_arr::Array{Int,1}, nameid::String)

    solver_name = "CPLEX"
    # solver_name = "GLPK"

    nb_simulation = length(l_arr)
    # Creating the dataframe that is going to be saved in a CSV at the end
    resultData = DataFrame(
            l = l_arr, c = c_arr, k = k_arr,
            maxc = maxc_arr, maxd = maxd_arr)
    resultData.costPL1 = Array{Int,1}(undef,nb_simulation)
    resultData.costPL2 = Array{Int,1}(undef,nb_simulation)
    resultData.nb_added_arcsPL2 = Array{Int64,1}(undef,nb_simulation)
    resultData.costPL3 = Array{Float64,1}(undef,nb_simulation)
    resultData.nb_added_arcsPL3 = Array{Int64,1}(undef,nb_simulation)
    resultData.costHeur = Array{Float64,1}(undef,nb_simulation)
    resultData.timePL1 = Array{Float64,1}(undef,nb_simulation)
    resultData.timePL2 = Array{Float64,1}(undef,nb_simulation)
    resultData.timePL3 = Array{Float64,1}(undef,nb_simulation)
    resultData.timeHeur = Array{Float64,1}(undef,nb_simulation)

    @showprogress 1 "Simulating..." for iter in 1:length(l_arr)
        # Generates the instance for the current iteration
        instance = generate(l_arr[iter], c_arr[iter], k_arr[iter], maxc_arr[iter], maxd_arr[iter])

        # Solving the first PL
        starttime = time()
        resultData.costPL1[iter] = create_pl1(solver_name, instance)
        resultData.timePL1[iter] = round(time()-starttime, digits = 3)

        # Solving the second PL
        starttime = time()
        pl2_cost, pl2_arc_nb = create_pl2(solver_name, instance)
        resultData.costPL2[iter] = pl2_cost
        resultData.nb_added_arcsPL2[iter] = pl2_arc_nb
        resultData.timePL2[iter] = round(time()-starttime, digits = 3)

        # Solving the third PL if the penalization is infinite
        if maxd_arr[iter] == -1
            starttime = time()
            cost, nb_arcs = create_pl3(solver_name, instance)
            resultData.costPL3[iter] = cost
            resultData.timePL3[iter] = round(time()-starttime, digits = 3)
            resultData.nb_added_arcsPL3[iter] = nb_arcs
        else
            resultData.costPL3[iter] = -1
            resultData.timePL3[iter] = round(time()-starttime, digits = 3)
            resultData.nb_added_arcsPL3[iter] = -1
        end

        # Running the heuristic
        starttime = time()
        cost, sol = heuristic(instance, 10, solver_name)
        resultData.costHeur[iter] = cost
        resultData.timeHeur[iter] = round(time()-starttime, digits = 3)
    end

    # Writing the results in a CSV
    CSV.write("../../res/simulate_params_$nameid.csv", resultData)
	return "simulate_params_$nameid.csv"
end

@doc """
    Preprocesses the data before the simulations
    params_lc[1] = initial constants of l_arr and c_arr
    params_lc[2] = steps of l_arr and c_arr
    params_k = (initial constant of k_arr, step of k_arr)
	params_maxc = (initial constant of maxc_arr, step of maxc_arr)
	params_maxd = (initial constant of maxd_arr, step of maxd_arr)
"""
function launch_simulate_time(nb_simulations::Int; params_lc::Tuple{Int,Int} = (10, 0),
     			  params_k::Tuple{Int,Int} = (3, 0),
				  params_maxc::Tuple{Int,Int} = (10, 0),
                  params_maxd::Tuple{Int,Int} = (10, 0))
    # Creating the arrays of parameters, each cell corresponding to a simulation
    l_arr = [params_lc[1] + params_lc[2]*i for i in 1:nb_simulations]
    c_arr = [params_lc[1] + params_lc[2]*i for i in 1:nb_simulations]
    k_arr = [params_k[1] + params_k[2]*i for i in 1:nb_simulations]
    maxc_arr = [params_maxc[1] + params_maxc[2]*i for i in 1:nb_simulations]
    maxd_arr = [params_maxd[1] + params_maxd[2]*i for i in 1:nb_simulations]

    # Setting the name for the final CSV
	nameid = ""
	if params_lc[2] > 0
		nameid = "variation_of_lc"
	elseif params_k[2] > 0
		nameid = "variation_of_k"
	elseif params_maxc[2] > 0
		nameid = "variation_of_maxc"
	elseif params_maxd[2] > 0
		nameid = "variation_of_maxd"
    end

    # simulate_time does the actual simulations
	return simulate_time(l_arr, c_arr, k_arr, maxc_arr, maxd_arr, nameid), nameid
end