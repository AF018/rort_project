using Statistics

@doc """
    This functions computes the performance of the heuristic nb_same_launch times, as well as the execution time for the heuristic :
        1) abs(opt - costHeuristique) / opt
        2) TimeHeuristique

    It computes these two values with each value of parameter p in p_arr
    length(p_arr) will be the number of simulations, nb_simulations

    l, c, k, maxc, and maxd or parameters of the instance

    Finaly, this functions write a CSV file with the mean of 1) and 2) with respect to nb_same_launch
"""
function simulate_param_p(p_arr::Array{Int,1}, l::Int, c::Int, k::Int, maxc::Int, maxd::Int, nb_same_launch::Int = 1)


    solver_name = "CPLEX"
    # solver_name = "GLPK"

    nb_simulations = length(p_arr)

    # output dataframe
    resultData = DataFrame(
            p = p_arr,
            l = [l for _ in 1:nb_simulations],
            c = [c for _ in 1:nb_simulations],
            k = [k for _ in 1:nb_simulations],
            maxc = [maxc for _ in 1:nb_simulations],
            maxd = [maxd for _ in 1:nb_simulations])

    #columns of dataframe
    resultData.timeHeur = Array{Float64,1}(undef,nb_simulations)
    resultData.distHeur = Array{Float64,1}(undef,nb_simulations)



    @showprogress 1 "Simulating..." for iter in 1:nb_simulations

        # timeHeur, distHeur
        mean_array = [[] for _ in 1:2]
        for restartidx in 1:nb_same_launch

            instance = generate(l, c, k, maxc, maxd)

            starttime = time()
            costpl1 = create_pl1(solver_name, instance)

            starttime = time()
            costH, sol = heuristic(instance, 10, solver_name)
            push!(mean_array[1], round(time()-starttime, digits = 3))
            push!(mean_array[2], abs(costH-costpl1)/costpl1)
        end

        mean_vect = round.(mean.(mean_array), digits = 4)
        resultData.timeHeur[iter] = mean_vect[1]
        resultData.distHeur[iter] = mean_vect[2]

    end

    outputfilename = "../../res/simulate_variation_of_p_$nb_simulations.csv"
    CSV.write(outputfilename, resultData)
	return outputfilename
end


@doc """
    Launches `simulate_param_p` with array p_arr generated thanks to params_p
    length of p_arr will be nb_simulations
"""
function launch_simulate_param_p(nb_simulations::Int,
                  nb_same_launch::Int = 1;
                  params_p::Tuple{Int,Int} = (5, 0),
                  l::Int = 10,
                  c::Int = 10,
                  k::Int = 3,
                  maxc::Int = 10,
                  maxd::Int = 10)
    p_arr = [params_p[1] + params_p[2]*i for i in 1:nb_simulations]

    return simulate_param_p(p_arr, l, c, k, maxc, maxd, nb_same_launch)
end
