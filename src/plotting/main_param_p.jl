using CSV
using DataFrames
using ProgressMeter
using PyPlot


include("../instances_io.jl")
include("../pl_p1.jl")
include("../heuristic.jl")
include("simulate_param_p.jl")
include("plotting_param_p.jl")


function main_param()

    # Number of simulations, different values of p, you might want to decrease that if you are using GLPK
    nb_simulations = 100

    # Number of time the heuristic will be launch, we'll plot mean of theses launches' results
    nb_same_launch = 10

    # (initial_value of p, step of p)
    # parameter p will be (10+i*1 for i in 1:nb_simulations)
    tuple_p = (10, 1)

    # parameter of instance
    l = 5

    # Launch simulation
    nb_same_launch = 10
    filename = launch_simulate_param_p(nb_simulations, nb_same_launch, params_p = tuple_p, l = l, c = l, k = 2, maxc = 5, maxd = 5)

    # Ploting and saving figures
    plot_time_param_p(filename, :p, :timeHeur)
    plot_cost_param_p(filename, :p, :distHeur)

end

main_param()
