using CSV
using DataFrames
using ProgressMeter
using Statistics

@doc """
    Completes the simulations recording the scores and saves the results in a CSV
    - All arrays in argument must have same length
    - nameid will be the end of output filename
"""
function simulate_cost(l_arr::Array{Int,1}, c_arr::Array{Int,1}, k_arr::Array{Int,1}, maxc_arr::Array{Int,1}, maxd_arr::Array{Int,1}, nameid::String, nb_same_launch::Int = 1)

    solver_name = "CPLEX"
    # solver_name = "GLPK"
    
    nb_simulation = length(l_arr)
    # Creating the dataframe that is going to be saved in a CSV at the end
    resultData = DataFrame(
            l = l_arr, c = c_arr, k = k_arr,
            maxc = maxc_arr, maxd = maxd_arr)
    resultData.costPL1 = Array{Int,1}(undef,nb_simulation)
    resultData.costPL2 = Array{Int,1}(undef,nb_simulation)
    resultData.costPL3 = Array{Float64,1}(undef,nb_simulation)
    resultData.nb_added_arcsPL3 = Array{Int64,1}(undef,nb_simulation)
    resultData.costHeur = Array{Float64,1}(undef,nb_simulation)
    resultData.timePL1 = Array{Float64,1}(undef,nb_simulation)
    resultData.timePL2 = Array{Float64,1}(undef,nb_simulation)
    resultData.timePL3 = Array{Float64,1}(undef,nb_simulation)
    resultData.timeHeur = Array{Float64,1}(undef,nb_simulation)
    resultData.distHeur = Array{Float64,1}(undef,nb_simulation)

    @showprogress 1 "Simulating..." for iter in 1:length(l_arr)
        # Array keeping the five following informations for every run
        # time PL1, time PL2, time PL3, time heuristic, dist between heuristic and optimum
        mean_array = [Array{Float64,1}(undef, 0) for _ in 1:5]
        for restartidx in 1:nb_same_launch

            instance = generate(l_arr[iter], c_arr[iter], k_arr[iter], maxc_arr[iter], maxd_arr[iter])

            # Solving the first PL
            starttime = time()
            costpl1 = create_pl1(solver_name, instance)
            push!(mean_array[1], round(time()-starttime, digits = 3))
            resultData.costPL1[iter] = costpl1

            # Solving the second PL
            starttime = time()
            pl2_cost, pl2_arc_nb = create_pl2(solver_name, instance)
            resultData.costPL2[iter] = pl2_cost

            # Solving the third PL if the penalization is infinite
            if maxd_arr[iter] == -1
                starttime = time()
                cost, nb_arcs = create_pl3(solver_name, instance)
                resultData.costPL3[iter] = cost
                resultData.timePL3[iter] = round(time()-starttime, digits = 3)
                resultData.nb_added_arcsPL3[iter] = nb_arcs
            else
                resultData.timePL3[iter] = round(time()-starttime, digits = 3)
                resultData.nb_added_arcsPL3[iter] = -1
                push!(mean_array[3], -1.0)
            end
            resultData.costPL3[iter] = -1.0
            resultData.costHeur[iter] = -1.0

            # Running the heuristic
            starttime = time()
            costH, sol = heuristic(instance, 10, solver_name)
            push!(mean_array[4], round(time()-starttime, digits = 3))
            push!(mean_array[5], abs(costH-costpl1)/costpl1)
        end

        # Computing the mean score of the runs
        mean_vect = round.(mean.(mean_array), digits = 4)
        resultData.timePL1[iter] = mean_vect[1]
        resultData.timePL2[iter] = mean_vect[2]
        resultData.timePL3[iter] = mean_vect[3]
        resultData.timeHeur[iter] = mean_vect[4]
        resultData.distHeur[iter] = mean_vect[5]
    end

    # Writing the results in a CSV
    CSV.write("../../res/simulate_params_cost_$nameid.csv", resultData)
	return "simulate_params_cost_$nameid.csv"
end


@doc """
    Preprocesses the data before the simulations
    params_lc[1] = initial constants of l_arr and c_arr
    params_lc[2] = steps of l_arr and c_arr
    params_k = (initial constant of k_arr, step of k_arr)
	params_maxc = (initial constant of maxc_arr, step of maxc_arr)
	params_maxd = (initial constant of maxd_arr, step of maxd_arr)
"""
function launch_simulate_cost(nb_simulations::Int,
                  nb_same_launch::Int = 1;
                  params_lc::Tuple{Int,Int} = (10, 0),
     			  params_k::Tuple{Int,Int} = (3, 0),
				  params_maxc::Tuple{Int,Int} = (10, 0),
				  params_maxd::Tuple{Int,Int} = (10, 0))
    # Creating the arrays of parameters, each cell corresponding to a simulation
    l_arr = [params_lc[1] + params_lc[2]*i for i in 1:nb_simulations]
    c_arr = [params_lc[1] + params_lc[2]*i for i in 1:nb_simulations]
    k_arr = [params_k[1] + params_k[2]*i for i in 1:nb_simulations]
    maxc_arr = [params_maxc[1] + params_maxc[2]*i for i in 1:nb_simulations]
    maxd_arr = [params_maxd[1] + params_maxd[2]*i for i in 1:nb_simulations]

    # Setting the name for the final CSV
    nameid = ""
	if params_lc[2] > 0
		nameid = "variation_of_lc"
	elseif params_k[2] > 0
		nameid = "variation_of_k"
	elseif params_maxc[2] > 0
		nameid = "variation_of_maxc"
	elseif params_maxd[2] > 0
		nameid = "variation_of_maxd"
	end

    # simulate_cost does the actual simulations
    return simulate_cost(l_arr, c_arr, k_arr, maxc_arr, maxd_arr, nameid, nb_same_launch), nameid
end
