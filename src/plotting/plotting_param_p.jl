


@doc """
    This functions plot the CSV file located in filepath with abscissa x_id and ordonate y_id, it's expected that y_id will represent execution time
    It also saves the figure in "../../res/plots/plot_time\$outputname.eps"
"""
function plot_time_param_p(filepath::String, x_id::Symbol, y_id::Symbol, outputname::String = "_param_p")

    data = CSV.read(filepath, header = true, delim = ',')


    fig = figure()
    plot(data[:,x_id], data[:,y_id], color = tuple(rand(3,1)...,), label=y_id)
    legend()
    suptitle("Execution time as a function of parameter $(x_id)")
    xlabel("$(x_id)")
    ylabel("Simulation time of $(y_id)")
    # show()
    if length(outputname) > 0
        savefig("../../res/plots/plot_time$outputname.eps")
    end
    close(fig)
end




@doc """
    This functions plot the CSV file located in filepath with abscissa x_id and ordonate y_id, it's expected that y_id will represent heuristic's performance
    It also saves the figure in "../../res/plots/plot_time\$outputname.eps"
"""
function plot_cost_param_p(filename::String, x_id::Symbol, heur_dist::Symbol,  outputname::String = "_param_p")

    data = CSV.read(filename, header = true, delim = ',')


    fig = figure()
    plot(data[:,x_id], data[:,heur_dist], color = tuple(rand(3,1)...,), label="|opt-cost(heuristic)|/opt")
    legend()
    title("Performance of heuristic as function of parameter $(x_id)")
    xlabel("$(x_id)")
    ylabel("|opt-cost(heuristic)|/opt")
    # show()
    if length(outputname) > 0
        savefig("../../res/plots/plot_cost$outputname.eps")
    end

    close(fig)
end
