using JuMP
using CPLEX
using GLPKMathProgInterface
using Random

include("instances_io.jl")

@doc """
    Solves the shortest path problem when given the instance, the penalized arcs and the name of the solver
"""
function shortest_path_h(instance::Data, x::Array{Bool,2}, solver::String = "CPLEX")
    # Choosing the solver
    if solver == "CPLEX"
        spath_m = Model(solver=CplexSolver(CPX_PARAM_SCRIND=0))
    end
    if solver == "GLPK"
        spath_m = Model(solver=GLPKSolverMIP())
    end

    n, k = instance.n, instance.k
    adj = instance.adj
    c = instance.c
    d = instance.d

    # Variables saying which arcs are used
    @variable(spath_m, arc[1:n, 1:n], Bin)

    # Minimize the weight of the path
    @objective(spath_m, Min, sum(arc[i,j]*(c[i][j] + d[i][j]*x[i,j]) for i in 1:n, j in 1:n))

    # Flow conversation constraint
    @constraint(spath_m, conservation[i=2:n-1], sum(adj[i][j]*arc[i,j] - adj[j][i]*arc[j,i] for j in 1:n) == 0)

    # Asking for one unit of flow out of the source and zero into it
    @constraint(spath_m, sum(adj[1][j]*arc[1,j] for j in 1:n) == 1)
    @constraint(spath_m, sum(adj[j][1]*arc[j,1] for j in 1:n) == 0)

    # Asking for one unit of flow into the source and zero out of it
    @constraint(spath_m, sum(adj[n][j]*arc[n,j] for j in 1:n) == 0)
    @constraint(spath_m, sum(adj[j][n]*arc[j,n] for j in 1:n) == 1)

    status = solve(spath_m)

    return getobjectivevalue(spath_m)
end

@doc """
    Generates p branches in the tree based on a partial solution
"""
function generate_branches(p::Int, ind_v::Int, nb_eliminated_arcs::Int, k::Int, m::Int)
    branches = [Array{Bool,1}(undef, 0) for i in 1:p]
    for i in 1:p
        for depth in ind_v+1:m
            # Goes on as long as we don't reach the bottom of the tree
            is_penalized = rand([true, false])
            nb_eliminated_arcs += is_penalized
            push!(branches[i], is_penalized)
            if nb_eliminated_arcs == k
                # Stops if the authorized number of arcs to penalize is reached
                break
            end

        end
    end
    return branches
end

@doc """
    Computes the mean cost of the given branches
"""
function compute_mean_cost(instance::Data, list_arcs::Array{Tuple{Int,Int}}, original_branch::Array{Bool,1}, branches::Array{Array{Bool,1},1}, solver::String)
    # Creates the variable saying which arcs have been penalized
    x = [false for _ in 1:instance.n, _ in 1:instance.n]
    nb_original_branches = length(original_branch)
    # Indicates the arcs that have been penalized in the partial solution
    for depth in 1:nb_original_branches
        x[list_arcs[depth][1], list_arcs[depth][2]] = original_branch[depth]
    end
    cost = 0.0
    for branch in branches
        # Adds the specific arcs penalized by the branch currently examined
        for depth in 1:length(branch)
            x[list_arcs[nb_original_branches+depth][1], list_arcs[nb_original_branches+depth][2]] = branch[depth]
        end
        cost += shortest_path_h(instance, x, solver)
    end
    # Returns the mean cost
    return cost/length(branches)
end

@doc """
    Function that performs the heuristic
"""
function heuristic(instance::Data, p::Int, solver::String)
    n, k, m = instance.n, instance.k, instance.m
    adj = instance.adj
    c = instance.c
    d = instance.d

    # Creates the vector indicating the order in which the arcs will be examined in the decision tree
    list_arcs = [(i, j) for i in 1:n for j in 1:n if adj[i][j]]
    # Shuffles it for a more random behavior
    shuffle!(list_arcs)

    ind_v = 1
    nb_eliminated_arcs = 0
    original_branch = Array{Bool,1}(undef,0)
    # Keep going as long as we have not reached the authorized number of penalized arcs of the bottom of th tree
    while (nb_eliminated_arcs < k) && (ind_v <= m)
        best_cost = -Inf
        is_penalized_child_node = true
        for is_penalized in [true, false]
            # Try the two children of the current node : penalized or not
            push!(original_branch, is_penalized)
            # Get the branches generated randomly from the current solution
            branches = generate_branches(p, ind_v, nb_eliminated_arcs + is_penalized, k, m)
            # Get the score for this child
            cost = compute_mean_cost(instance, list_arcs, original_branch, branches, solver)
            pop!(original_branch)
            # Keep the best of two children
            if best_cost < cost
                is_penalized_child_node = is_penalized
                best_cost = cost
            end
        end
        # Update the number of currently penalized arcs
        if is_penalized_child_node
            nb_eliminated_arcs += 1
        end
        push!(original_branch, is_penalized_child_node)
        ind_v += 1

    end
    # display_branch(original_branch, list_arcs)
    return compute_mean_cost(instance, list_arcs, original_branch, [Array{Bool,1}(undef,0)], solver), original_branch
end

function display_branch(branch::Array{Bool,1}, list_arcs::Array{Tuple{Int,Int}})
    for arc_ind in 1:length(branch)
        if branch[arc_ind]
            println(list_arcs[arc_ind][1], " ", list_arcs[arc_ind][2])
        end
    end
end
