using JuMP
using CPLEX
using GLPKMathProgInterface

include("instances_io.jl")
@doc """
    Computes shortest path
"""
function shortest_path_pl3(instance::Data, x::Array{Bool,2}, solver::String = "CPLEX")
    if solver == "CPLEX"
        spath_m = Model(solver=CplexSolver(CPX_PARAM_SCRIND=0))
    end
    if solver == "GLPK"
        spath_m = Model(solver=GLPKSolverMIP())
    end

    n::Int, k::Int = instance.n, instance.k
    adj = instance.adj
    c = instance.c
    d = instance.d

    # Variables saying which arcs are used
    @variable(spath_m, arc[1:n, 1:n], Bin)

    # Minimize the weight of the path
    @objective(spath_m, Min, sum(arc[i,j]*(c[i][j] + d[i][j]*x[i,j]) for i in 1:n, j in 1:n))

    # Flow conversation constraint
    @constraint(spath_m, conservation[i=2:n-1], sum(adj[i][j]*arc[i,j] - adj[j][i]*arc[j,i] for j in 1:n) == 0)

    # Asking for one unit of flow out of the source and zero into it
    @constraint(spath_m, sum(adj[1][j]*arc[1,j] for j in 1:n) == 1)
    @constraint(spath_m, sum(adj[j][1]*arc[j,1] for j in 1:n) == 0)

    # Asking for one unit of flow into the source and zero out of it
    @constraint(spath_m, sum(adj[n][j]*arc[n,j] for j in 1:n) == 0)
    @constraint(spath_m, sum(adj[j][n]*arc[j,n] for j in 1:n) == 1)

    status = solve(spath_m)
    m_shortestp = getvalue(arc)
    # println("Objective value: ", getobjectivevalue(spath_m))

    return m_shortestp, getobjectivevalue(spath_m)
end

@doc """
    Solves instance with PL3
    Returns objective value and number of added arcs
"""
function create_pl3(solver, instance::Data)
    if solver == "CPLEX"
        pl3_m = Model(solver=CplexSolver(CPX_PARAM_SCRIND=0))
    elseif solver == "GLPK"
        pl3_m = Model(solver=GLPKSolverMIP())
    end

    n, k = instance.n, instance.k
    adj = instance.adj
    c = instance.c
    d = instance.d

    # Variable name corresponds to its letter in the LP
    @variable(pl3_m, x[1:n,1:n], Bin)

    current_sol = [false for _ in 1:n, _ in 1:n]
    best_spvalue = 0.0
    # Getting an initial solution for the subproblem to add to the constraints
    Y, spvalue = shortest_path_pl3(instance, [false for _ in 1:n, _ in 1:n], solver)

    @constraint(pl3_m, sum(x[u,v] for u in 1:n, v in 1:n if Y[u,v] == 1) >= 1)

    # Number of penalized selected arcs
    @objective(pl3_m, Min, sum(x[i,j] for i in 1:n, j in 1:n))

    status = solve(pl3_m)
    current_objective_value = getobjectivevalue(pl3_m)

    current_x = getvalue(x)
    current_x = convert(Array{Bool,2}, current_x)

    Y, spvalue = shortest_path_pl3(instance, current_x, solver)
    count_nb_added_paths = 1
    maxc = maximum(maximum.(instance.c))
    # Keep going as long as the graph is not disconnected and we have less than k penalized arcs
    while current_objective_value <= k && best_spvalue <= maxc * instance.m + 1
        count_nb_added_paths += 1
        # Add the constraint corresponding to the path just found
        @constraint(pl3_m, sum(x[u,v] for u in 1:n, v in 1:n if Y[u,v] == 1) >= 1)

        # Solve the master problem again
        solve(pl3_m)
        current_objective_value = getobjectivevalue(pl3_m)

        current_x = getvalue(x)
        current_x = convert(Array{Bool,2}, round.(current_x))

        if spvalue >= best_spvalue
            best_spvalue = spvalue
            current_sol = deepcopy(current_x)
        end
        # Compute a shortest path in the updated graph
        Y, spvalue = shortest_path_pl3(instance, current_x, solver)

    end

    # println("Objective value: ", getobjectivevalue(pl3_m))

    # @show maxc*instance.m + 1
    # if (best_spvalue >= maxc*instance.m + 1)
    #     println("Objective value is Inf")
    # else
    #     println("Objective value P3: ", best_spvalue)
    # end

    return best_spvalue, count_nb_added_paths

end
