using JuMP
using CPLEX
using GLPKMathProgInterface

include("instances_io.jl")

@doc """
    Computes shortest path
"""
function shortest_path_pl2(instance::Data, x::Array{Bool,2}, solver::String = "CPLEX")
    if solver == "CPLEX"
        spath_m = Model(solver=CplexSolver(CPX_PARAM_SCRIND=0))
    end
    if solver == "GLPK"
        spath_m = Model(solver=GLPKSolverMIP())
    end

    n::Int, k::Int = instance.n, instance.k
    adj = instance.adj
    c = instance.c
    d = instance.d

    # Variables saying which arcs are used
    @variable(spath_m, arc[1:n, 1:n], Bin)

    # Minimize the weight of the path
    @objective(spath_m, Min, sum(arc[i,j]*(c[i][j] + d[i][j]*x[i,j]) for i in 1:n, j in 1:n))

    # Flow conversation constraint
    @constraint(spath_m, conservation[i=2:n-1], sum(adj[i][j]*arc[i,j] - adj[j][i]*arc[j,i] for j in 1:n) == 0)

    # Asking for one unit of flow out of the source and zero into it
    @constraint(spath_m, sum(adj[1][j]*arc[1,j] for j in 1:n) == 1)
    @constraint(spath_m, sum(adj[j][1]*arc[j,1] for j in 1:n) == 0)

    # Asking for one unit of flow into the source and zero out of it
    @constraint(spath_m, sum(adj[n][j]*arc[n,j] for j in 1:n) == 0)
    @constraint(spath_m, sum(adj[j][n]*arc[j,n] for j in 1:n) == 1)

    status = solve(spath_m)
    m_shortestp = getvalue(arc)
    #println("Objective value: ", getobjectivevalue(spath_m))

    return m_shortestp, getobjectivevalue(spath_m)
end


@doc """
    Solves instance with PL2
    Returns objective value and number of added arcs
"""
function create_pl2(solver::String, instance::Data)
    if solver == "CPLEX"
        #### CPX_PARAM_TILIM=1*60 Stopping CPLEX after 1 minute
        pl2_m = Model(solver=CplexSolver(CPX_PARAM_SCRIND=0, CPX_PARAM_TILIM=60))
    end
    if solver == "GLPK"
        pl2_m = Model(solver=GLPKSolverMIP())
    end

    n, k = instance.n, instance.k
    adj = instance.adj
    c = instance.c
    d = instance.d

    # Variable names correspond to their letter in the LP
    @variable(pl2_m, z)
    @variable(pl2_m, x[1:n,1:n], Bin)

    @objective(pl2_m, Max, z)

    Y, spvalue = shortest_path_pl2(instance, [false for _ in 1:n, _ in 1:n], solver)

    # Getting an initial solution for the subproblem to add to the constraints
    @constraint(pl2_m, sum(c[i][j] + d[i][j]*x[i,j]
                for i in 1:n, j in 1:n if Y[i,j] == 1) >= z)
    added_arcs_nb = 1

    # Number of penalized selected arcs
    @constraint(pl2_m, sum(x[i,j] for i in 1:n, j in 1:n) <= k)

    status = solve(pl2_m)
    current_z = getobjectivevalue(pl2_m)

    current_x = getvalue(x)
    current_x = convert(Array{Bool,2}, current_x)

    Y, spvalue = shortest_path_pl2(instance, current_x, solver)

    # Keep going as long as solving the subproblem doesn't bring a solution violating the global problem
    while current_z > spvalue
        # Add the constraint corresponding to the violating solution 
        @constraint(pl2_m, sum(c[i][j] + d[i][j]*x[i,j]
                    for i in 1:n, j in 1:n if Y[i,j] == 1) >= z)
        added_arcs_nb += 1

        # Solve the master problem again
        solve(pl2_m)
        current_z = getobjectivevalue(pl2_m)

        current_x = getvalue(x)
        current_x = convert(Array{Bool,2}, round.(current_x))
        # Compute a shortest path in the updated graph, to see if we find a violating solution
        Y, spvalue = shortest_path_pl2(instance, current_x, solver)

    end

    # println("")
    # println("Set of disconnecting arcs : ")
    # for i in 1:n
    #     for j in 1:n
    #         if (getvalue(x[i,j]) >= 1)
    #             println(string("(",i,",",j,")"))
    #         end
    #     end
    # end

    obj_value = convert(Int, round(getobjectivevalue(pl2_m)))

    maxc = maximum(maximum.(instance.c))
    # if (obj_value >= maxc*instance.m + 1)
    #     println("Objective value is Inf")
    # else
    #     println("Objective value: ", getobjectivevalue(pl2_m))
    # end

    return obj_value, added_arcs_nb

end
