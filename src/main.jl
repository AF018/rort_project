include("instances_io.jl") # WARNING this file has lightly modified
include("pl_p1.jl")
include("pl_p2.jl")
include("pl_p3.jl")
include("heuristic.jl")

function main()
    ########### Parameters of instance
    # l, c, k, maxc, maxd = 10, 10, 3, 10, -1
    l, c, k, maxc, maxd = 10, 10, 3, 10, 10

    ########### Generating instance
    instance = generate(l, c, k, maxc, maxd)

    solver_name = "CPLEX"
    # solver_name = "GLPK"

    ########### Launching solving
    starttime = time()
    pl1_value = create_pl1(solver_name, instance)
    println("Total time for PL1: ", time()-starttime)

    starttime = time()
    pl2_value, count_nb_added_paths_pl2 = create_pl2(solver_name, instance)
    println("Total time for PL2: ", time()-starttime)

    if maxd == -1
        starttime = time()
        pl3_value, count_nb_added_paths_pl3 = create_pl3(solver_name, instance)
        println("Total time for PL3: ", time()-starttime)
    end

    println()
    @show pl1_value
    @show pl2_value
    @show count_nb_added_paths_pl2
    if maxd == -1
        @show pl3_value
        @show count_nb_added_paths_pl3
    end

    starttime = time()
    cost, sol = heuristic(instance, 10, solver_name)
    println("Total time for heuristic: ", time()-starttime)
    println("Final cost : ", cost)
end

main()
