using JuMP
using CPLEX
using GLPKMathProgInterface

include("instances_io.jl")

@doc """
    Solves instance with PL1
    Returns objective value
"""
function create_pl1(solver::String, instance::Data)

    if solver == "CPLEX"
        pl1_model = Model(solver=CplexSolver(CPX_PARAM_SCRIND=0, CPX_PARAM_TILIM=40))
    end
    if solver == "GLPK"
        pl1_model = Model(solver=GLPKSolverMIP())
    end

    n, k = instance.n, instance.k
    adj = instance.adj
    c = instance.c
    d = instance.d

    # Variable names correspond to their letter in the LP 
    @variable(pl1_model, pi[1:n])
    @variable(pl1_model, x[1:n,1:n], Bin)

    @objective(pl1_model, Max, pi[n] - pi[1])
    for i in 1:n
        for j in 1:n
            if adj[i][j]
                @constraint(pl1_model, pi[j] - pi[i] - d[i][j]*x[i,j] <= c[i][j])
            end
        end
    end

    @constraint(pl1_model, pi[1] == 0)
    @constraint(pl1_model, sum(x[i,j] for i in 1:n, j in 1:n) <= k)

    status = solve(pl1_model)

    # println("")
    # println("Set of disconnecting arcs : ")
    # for i in 1:n
    #     for j in 1:n
    #         if (getvalue(x[i,j]) >= 1)
    #             println(string("(",i,",",j,")"))
    #         end
    #     end
    # end

    # Retrieve the objective value
    obj_value = convert(Int, round(getobjectivevalue(pl1_model)))

    maxc = maximum(maximum.(instance.c))
    # if (obj_value >= maxc*instance.m + 1)
    #     println("Objective value is Inf")
    # else
    #     println("Objective value: ", getobjectivevalue(pl1_model))
    # end

    return obj_value
end
