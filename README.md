# RORT Project

Projet du cours « RO, Réseaux et Transports » du MPRO

#### Professeurs et auteurs


* _Dimitri Watel_
* _Alain Faye_

Irina Legkikh
- Antoine François - Julien Khamphousone





# Contenu

Ce projet contient $2$ dossiers

* `src` : qui contient les sources du projets ainsi qu'un sous dossier `plotting` qui nous a permis de générer toutes les figures présentes dans le rapport.
* `res` : qui est vide mais qui se remplit lorsque l'on lance les fichiers main du sous dossier `plotting`

## Fichier instance

Nous avons modifié le fichier `instances_io`. Le résultat des fonctions `generate` est le même mais les performances sont meilleures avec la version que nous avons installée.

# Compilation et exécution


## Main principal

Pour lancer le code, utiliser dans le dossier `src` :

`julia -qi ./main.jl`


## Main plotting

Pour lancer la création des plots, lancer dans le dossier `plotting` :


* `julia -qi ./main_plotting.jl`
* `julia -qi ./main_param_p.jl`



### Warning, si vous ne voulez absolument pas utiliser CPLEX (non recommandé)

_**Warning:** Vous purriez souhaiter changer le solveur utilisé (par défaut CPLEX), il faut pour ceci modifier la variable `solver_name` dans tous les fichiers : _ `src/plotting/simulate*.pl`

Il est également possible que GLPK ne soit pas assez performant pour plotter tous les plots du rapport (voire aucun)... Les paramètres dans les différents main sont ainsi à changer.
